const std = @import("std");

test "split iterator" {
    const text = "robust, optimal, reusable, maintainable, ";
    var iter = std.mem.split(text, ", ");

    std.testing.expect(std.mem.eql(u8, iter.next().?, "robust"));
    std.testing.expect(std.mem.eql(u8, iter.next().?, "optimal"));
    std.testing.expect(std.mem.eql(u8, iter.next().?, "reusable"));
    std.testing.expect(std.mem.eql(u8, iter.next().?, "maintainable"));
    std.testing.expect(std.mem.eql(u8, iter.next().?, ""));
    std.testing.expect(iter.next() == null);
}

// !?T
test "iterator looping" {
    var iter = (try std.fs.cwd().openDir(".", .{ .iterate = true })).iterate();

    var file_count: u32 = 0;
    while (try iter.next()) |entry| {
        if (entry.kind == .File) file_count += 1;
    }

    std.debug.print("number of files in this directory = {}\n", .{file_count});
}

// ?!T
test "arg iteration" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    var arg_characters: usize = 0;
    var iter = std.process.args();

    while (iter.next(allocator)) |arg| {
        const argument = arg catch break;
        std.debug.print("{s}\n", .{argument});
        arg_characters += argument.len;
        allocator.free(argument);
    }

    std.debug.print("you entered {} arg characters\n", .{arg_characters});
}
