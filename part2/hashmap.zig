const std = @import("std");
const debug = std.debug;
const testing = std.testing;

const Point = struct {
    x: i32,
    y: i32,

    pub fn init(x: i32, y: i32) Point {
        return Point{
            .x = x,
            .y = y,
        };
    }
};

test "hashmaps" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    var map = std.AutoHashMap(u32, Point).init(allocator);
    defer map.deinit();

    try map.put(1, Point.init(100, 200));
    try map.put(2, Point.init(99, 300));
    try map.put(3, Point.init(200, -12345));
    try map.put(4, Point.init(-1990, 300));
    try map.put(5, Point.init(120, -30));

    testing.expectEqual(map.count(), 5);

    var sum = Point.init(0, 0);
    var iter = map.iterator();

    while (iter.next()) |entry| {
        sum.x += entry.value.x;
        sum.y += entry.value.y;
    }

    testing.expectEqual(sum.x, -1471);
    testing.expectEqual(sum.y, -11575);
}

// fetchPut inserts a value for the key, returning the old value (entry) of the key, if available
test "fetchPut" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    var map = std.AutoHashMap(u8, f32).init(allocator);
    defer map.deinit();

    try map.put(255, 10);
    testing.expectEqual(map.get(255).?, 10);

    const old_val = try map.fetchPut(255, 11);
    testing.expectEqual(old_val.?.value, 10);
    testing.expectEqual(map.get(255).?, 11);
}

// a specialised version for string keys
test "string hashmaps" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    var map = std.StringHashMap(enum { cool, uncool }).init(allocator);
    defer map.deinit();

    try map.put("albert", .uncool);
    try map.put("trebla", .cool);

    testing.expectEqual(map.get("albert").?, .uncool);
    testing.expectEqual(map.get("trebla").?, .cool);

    var iter = map.iterator();
    while (iter.next()) |kv| {
        debug.print("{s} => {}\n", .{ kv.key, kv.value });
    }
}
