const std = @import("std");
const ArrayList = std.ArrayList;
const debug = std.debug;
const testing = std.testing;

test "arraylist" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    var list = ArrayList(i32).init(&gpa.allocator);
    defer list.deinit();

    for ([_]i32{ 1, 2, 3, 4, 5 }) |n| {
        try list.append(n);
    }

    testing.expectEqual(list.items.len, 5);
    displayList(i32, list);
}

fn displayList(comptime T: type, list: ArrayList(T)) void {
    for (list.items) |e| {
        debug.print("{} ", .{e});
    }
    debug.print("\n", .{});
}

test "matrix using ArrayList" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    var mat = ArrayList(ArrayList(i32)).init(&gpa.allocator);
    defer mat.deinit();
    defer {
        for (mat.items) |row| row.deinit();
    }

    for ([2][3]i32{ [_]i32{ 1, 2, 3 }, [_]i32{ 4, 5, 6 } }) |row| {
        var row_list = ArrayList(i32).init(&gpa.allocator);
        try row_list.appendSlice(&row);
        try mat.append(row_list);
    }

    displayMatrix(i32, mat);
}

fn displayMatrix(comptime T: type, mat: ArrayList(ArrayList(T))) void {
    for (mat.items) |row| {
        for (row.items) |e| {
            debug.print("{} ", .{e});
        }
        debug.print("\n", .{});
    }
}
