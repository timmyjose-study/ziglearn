const std = @import("std");
const debug = std.debug;

const Person = struct {
    name: []const u8,
    birth_year: i32,
    death_year: ?i32,

    pub fn init(name: []const u8, birth_year: i32, death_year: ?i32) Person {
        return Person{
            .name = name,
            .birth_year = birth_year,
            .death_year = death_year,
        };
    }

    // "debug" formatting or default formatting still works - this method is needed for custom formatting
    pub fn format(self: Person, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        debug.print("{s}\n", .{@typeName(@TypeOf(writer))});
        try writer.print("{s} ({} - ", .{ self.name, self.birth_year });

        if (self.death_year) |year| {
            try writer.print("{}", .{year});
        }

        try writer.print(")", .{});
    }
};

test "custom fmt" {
    const bob = Person.init("Bob Davis", 1992, null);

    const stdout = std.io.getStdOut();
    try stdout.writer().print("{s}\n", .{bob});

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    const bob_string = try std.fmt.allocPrint(allocator, "{s}\n", .{bob});
    defer allocator.free(bob_string);

    debug.print("{s}\n", .{bob_string});
    debug.print("{any}\n", .{bob_string});
    debug.print("{*}\n", .{bob_string});
}
