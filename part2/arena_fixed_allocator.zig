const std = @import("std");
const debug = std.debug;

pub fn main() !void {
    var buffer: [1024]u8 = undefined;
    var arena = std.heap.ArenaAllocator.init(&std.heap.FixedBufferAllocator.init(&buffer).allocator);
    defer arena.deinit();

    var allocator = &arena.allocator;
    const mem = try allocator.alloc(u8, 100);
    debug.print("len = {}, type = {s}\n", .{ mem.len, @typeName(@TypeOf(mem)) });
}
