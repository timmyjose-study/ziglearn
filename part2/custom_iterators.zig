const std = @import("std");
const testing = std.testing;

const ContainsIterator = struct {
    strings: []const []const u8,
    needle: []const u8,
    index: usize = 0,

    pub fn next(self: *ContainsIterator) ?[]const u8 {
        for (self.strings[self.index..]) |string| {
            self.index += 1;
            if (std.mem.indexOf(u8, string, self.needle)) |_| {
                return string;
            }
        }
        return null;
    }
};

test "custom iterator" {
    var iter = ContainsIterator{
        .strings = &[_][]const u8{ "one", "two", "three", "four", "five" },
        .needle = "e",
    };

    testing.expect(std.mem.eql(u8, iter.next().?, "one"));
    testing.expect(std.mem.eql(u8, iter.next().?, "three"));
    testing.expect(std.mem.eql(u8, iter.next().?, "five"));
    testing.expect(iter.next() == null);
}
