const std = @import("std");
const fs = std.fs;
const mem = std.mem;
const ArrayList = std.ArrayList;
const debug = std.debug;
const testing = std.testing;

test "io writer" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    var list = ArrayList(u8).init(&gpa.allocator);
    defer list.deinit();

    // list exposes a Writer implmentation
    const bytes_written = try list.writer().write("Hello, world!");
    testing.expectEqual(bytes_written, "Hello, world!".len);
    testing.expect(mem.eql(u8, list.items, "Hello, world!"));
}

test "io reader usage" {
    const message = "Hello, world!";
    const file = try fs.cwd().createFile("temp.txt", .{ .read = true });
    defer {
        fs.cwd().deleteFile("temp.txt") catch unreachable;
    }
    defer file.close();

    try file.writeAll(message);
    try file.seekTo(0);

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;
    const contents = try file.reader().readAllAlloc(allocator, message.len);
    defer allocator.free(contents);
    testing.expectEqual(contents.len, message.len);
    testing.expect(mem.eql(u8, contents, "Hello, world!"));
}

fn nextLine(reader: @TypeOf(std.io.getStdIn().reader()), buffer: []u8) !?[]const u8 {
    var line = (try reader.readUntilDelimiterOrEof(buffer, '\n')) orelse return null;
    if (std.builtin.Target.current.os.tag == .windows) {
        line = mem.trimRight(u8, line, "\r");
    }

    return line;
}

test "read from stdin" {
    const stdout = std.io.getStdOut();
    const stdin = std.io.getStdIn();

    try stdout.writeAll("What is your name? ");
    var buffer: [1024]u8 = undefined;
    const input = (try nextLine(stdin.reader(), &buffer)).?;
    try stdout.writer().print("Nice to meet you, {s}\n", .{input});
}
