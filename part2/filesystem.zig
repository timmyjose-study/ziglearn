const std = @import("std");
const mem = std.mem;
const fs = std.fs;
const debug = std.debug;
const testing = std.testing;

test "createFile, write, seekTo, read" {
    const file = try fs.cwd().createFile("hello.txt", .{ .read = true });
    defer file.close();

    _ = try file.writeAll("Hello, world!");

    var buffer: [1024]u8 = undefined;
    try file.seekTo(0);
    const len = try file.readAll(&buffer);
    testing.expect(mem.eql(u8, buffer[0..len], "Hello, world!"));
    debug.print("{s}\n", .{buffer[0..len]});
}

test "file stat" {
    const file = try fs.cwd().openFile("hello.txt", .{ .read = true });
    defer {
        fs.cwd().deleteFile("hello.txt") catch unreachable;
    }
    defer file.close();

    const stat = try file.stat();
    debug.print("size = {}, kind = {}\n", .{ stat.size, stat.kind });
    debug.print("ctime = {}, atime = {}, mtime = {}\n", .{ stat.ctime, stat.atime, stat.mtime });
}

test "make dir" {
    try fs.cwd().makeDir("temp-dir");
    const dir = try fs.cwd().openDir("temp-dir", .{ .iterate = true });

    defer {
        fs.cwd().deleteTree("temp-dir") catch unreachable;
    }

    _ = try dir.createFile("x", .{});
    _ = try dir.createFile("y", .{});
    _ = try dir.createFile("z", .{});

    var file_count: usize = 0;
    var iter = dir.iterate();

    while (try iter.next()) |entry| {
        if (entry.kind == .File) file_count += 1;
    }
    testing.expectEqual(file_count, 3);
}
