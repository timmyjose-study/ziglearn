const std = @import("std");
const fmt = std.fmt;
const ArrayList = std.ArrayList;
const debug = std.debug;

fn fmtDemo() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    const string = try fmt.allocPrint(allocator, "{d} + {d} = {d}", .{ 1, 2, 3 });
    defer allocator.free(string);

    debug.print("{s}\n", .{string});
}

fn printDemo() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    var list = ArrayList(u8).init(allocator);
    defer list.deinit();

    try list.writer().print("{d} + {d} = {d}", .{ 1, 2, 3 });
    debug.print("{s}\n", .{list.items});
}

fn helloWorld() !void {
    const stdout = std.io.getStdOut();
    try stdout.writer().print("Hello, {s}\n", .{"world"});
}

// {s} gives string formatting. {any} gives the default formatting (debug)
fn arrayPrinting() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    const string = try fmt.allocPrint(allocator, "{any} + {any} = {any}", .{
        @as([]const u8, &[_]u8{ 1, 2 }),
        @as([]const u8, &[_]u8{ 3, 4 }),
        @as([]const u8, &[_]u8{ 4, 6 }),
    });

    defer allocator.free(string);

    debug.print("string = {s}\n", .{string});
    debug.print("string = {any}\n", .{string});
}

pub fn main() void {
    fmtDemo() catch {};
    printDemo() catch {};
    helloWorld() catch {};
    arrayPrinting() catch {};
}
