const std = @import("std");

test "sorting" {
    var data = [_]i32{ 5, 2, 1, 5, 4, 3 };
    std.sort.sort(i32, &data, {}, comptime std.sort.asc(i32));
    std.testing.expect(std.meta.eql([_]i32{ 1, 2, 3, 4, 5, 5 }, data));

    std.sort.sort(i32, &data, {}, comptime std.sort.desc(i32));
    std.testing.expect(std.meta.eql([_]i32{ 5, 5, 4, 3, 2, 1 }, data));
}
