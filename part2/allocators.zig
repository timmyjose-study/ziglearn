const std = @import("std");
const testing = std.testing;

test "basic allocation" {
    var allocator = std.heap.page_allocator;
    var mem = try allocator.alloc(u8, 100);
    defer allocator.free(mem);

    testing.expectEqual(mem.len, 100);
    testing.expectEqual(@TypeOf(mem), []u8);
}

test "fixed buffer allocator" {
    var buffer: [1000]u8 = undefined;
    var fba = std.heap.FixedBufferAllocator.init(&buffer);
    var allocator = &fba.allocator;

    const mem = try allocator.alloc(u8, 100);
    defer allocator.free(mem); // not really needed since it's stack allocated
    testing.expectEqual(mem.len, 100);
    testing.expectEqual(@TypeOf(mem), []u8);
}

test "arena allocator" {
    //var buffer: [1024]u8 = undefined;
    //var arena = std.heap.ArenaAllocator.init(&std.heap.FixedBufferAllocator.init(&buffer).allocator);
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var allocator = &arena.allocator;

    const mem = try allocator.alloc(u8, 100);
    testing.expectEqual(mem.len, 100);
    testing.expectEqual(@TypeOf(mem), []u8);
}

// for slices, use alloc and free, and for single-items, use create and destroy
test "create destroy" {
    const byte = try std.heap.page_allocator.create(u8);
    defer std.heap.page_allocator.destroy(byte);
    byte.* = 42;
    testing.expectEqual(byte.*, 42);
}

test "general-purpose allocator" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        const leaked = gpa.deinit();
        if (leaked) unreachable;
    }

    const mem = try gpa.allocator.alloc(u8, 100);
    defer gpa.allocator.free(mem);
    testing.expectEqual(mem.len, 100);
    testing.expectEqual(@TypeOf(mem), []u8);
}
