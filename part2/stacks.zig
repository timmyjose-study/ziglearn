const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "stacks using ArrayList" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    var allocator = &gpa.allocator;

    const string = "(()())";
    var stack = std.ArrayList(usize).init(allocator);
    defer stack.deinit();

    const Pair = struct { open: usize, close: usize };
    var pairs = std.ArrayList(Pair).init(allocator);
    defer pairs.deinit();

    for (string) |char, idx| {
        switch (char) {
            '(' => try stack.append(idx),
            ')' => try pairs.append(Pair{ .open = stack.pop(), .close = idx }),
            else => {},
        }
    }

    for (pairs.items) |pair, idx| {
        testing.expect(std.meta.eql(pair, switch (idx) {
            0 => Pair{ .open = 1, .close = 2 },
            1 => Pair{ .open = 3, .close = 4 },
            2 => Pair{ .open = 0, .close = 5 },
            else => unreachable,
        }));
    }
}
