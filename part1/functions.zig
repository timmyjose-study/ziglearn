const std = @import("std");
const testing = std.testing;

fn addFive(comptime T: type, x: T) T {
    return x + 5;
}

test "functions" {
    testing.expectEqual(addFive(i32, 10), 15);
}

fn fibonacci(n: i32) i32 {
    if (n == 0) {
        return 0;
    } else if (n == 1) {
        return 1;
    } else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

test "fibonacci" {
    testing.expectEqual(fibonacci(0), 0);
    testing.expectEqual(fibonacci(1), 1);
    testing.expectEqual(fibonacci(2), 1);
    testing.expectEqual(fibonacci(3), 2);
    testing.expectEqual(fibonacci(4), 3);
    testing.expectEqual(fibonacci(10), 55);
}
