const std = @import("std");
const debug = std.debug;
const testing = std.testing;

test "for" {
    const message = "Hello, world!";

    for (message) |c| {
        debug.print("{u} ", .{c});
    }
    debug.print("\n", .{});
}

test "display array" {
    const arr = [_]i32{ 1, 2, 3, 4, 5 };
    displayArray(i32, arr[0..]);
}

test "for with mutation" {
    var matrix = [2][3]i32{ [_]i32{ 1, 2, 3 }, [_]i32{ 4, 5, 6 } };
    var sum: i32 = 0;

    for (matrix) |row| {
        for (row) |e| {
            sum += e;
        }
    }
    testing.expectEqual(sum, 21);

    for (matrix) |*row| {
        for (row) |*e| {
            e.* += 100;
        }
    }

    displayMatrix([2][3]i32, matrix);
}

fn displayArray(comptime T: type, arr: []const T) void {
    for (arr) |e| {
        debug.print("{} ", .{e});
    }
    debug.print("\n", .{});
}

fn displayMatrix(comptime T: type, mat: T) void {
    for (mat) |r| {
        for (r) |e| {
            debug.print("{} ", .{e});
        }
        debug.print("\n", .{});
    }
}
