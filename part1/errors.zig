const std = @import("std");
const testing = std.testing;

const FileOpenError = error{ AccessDenied, OutOfMemory, FileNotFound };

const AllocationError = error{OutOfMemory};

test "coerce subset to superset" {
    const err: FileOpenError = AllocationError.OutOfMemory;
    testing.expectEqual(err, FileOpenError.OutOfMemory);
}

test "error union" {
    const maybe_error: AllocationError!u32 = 10;
    const no_error = maybe_error catch 0;
    testing.expectEqual(@TypeOf(no_error), u32);
    testing.expectEqual(no_error, 10);
}

fn failingFunction() error{Oops}!void {
    return error.Oops;
}

test "catching an error" {
    failingFunction() catch |err| {
        testing.expectEqual(err, error.Oops);
        return;
    };
}

fn anotherFailingFunction(b: bool) error{Oops}!u32 {
    return if (b) 42 else error.Oops;
}

test "try-catch" {
    const res1 = anotherFailingFunction(true) catch |err| return err;
    testing.expectEqual(res1, 42);

    // same as above
    const res2 = try anotherFailingFunction(true);
    testing.expectEqual(res2, 42);

    _ = try anotherFailingFunction(false) 
}


