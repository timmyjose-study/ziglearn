const std = @import("std");
const testing = std.testing;

pub fn main() !void {
    const foo: i32 = 42;
    var bar: i32 = 42;
    testing.expectEqual(foo, bar);

    const inferred_constant = @as(i32, 42);
    var inferred_variable = @as(i32, 42);
    testing.expectEqual(inferred_constant, inferred_constant);
}
