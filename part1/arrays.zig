const std = @import("std");
const mem = std.mem;
const debug = std.debug;
const testing = std.testing;

test "arrays" {
    const arr1 = [_]i32{ 1, 2, 3, 4, 5 };
    const arr2 = [5]i32{ 1, 2, 3, 4, 5 };
    testing.expect(mem.eql(i32, &arr1, &arr2));
    testing.expectEqual(arr1.len, arr2.len);
    testing.expectEqual(@TypeOf(arr1), @TypeOf(arr2));
    testing.expectEqual(@TypeOf(arr1), [5]i32);
    debug.print("{s}\n", .{@typeName(@TypeOf(arr1))});

    for (arr1) |e| {
        debug.print("{} ", .{e});
    }
    debug.print("\n", .{});
}
