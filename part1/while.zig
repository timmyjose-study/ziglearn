const std = @import("std");
const testing = std.testing;

test "while" {
    var f: u32 = 1;
    var i: u32 = 1;

    while (i <= 5) {
        f *= i;
        i += 1;
    }

    testing.expectEqual(f, 120);
}

test "while with continue expression" {
    var f: u32 = 1;
    var i: u32 = 1;

    while (i <= 5) : ({
        f *= i;
        i += 1;
    }) {}
    testing.expectEqual(f, 120);
}

test "while with continue" {
    var ctr: u32 = 1;
    var sum: u32 = 0;

    while (ctr < 10) : (ctr += 1) {
        if (@rem(ctr, 2) == 0) continue;
        sum += ctr;
    }
    testing.expectEqual(sum, 25);
}

test "while with break" {
    var ctr: u32 = 1;
    var sum: u32 = 0;

    while (ctr < 10) : (ctr += 1) {
        if (ctr > 5) break;
        sum += ctr;
    }
    testing.expectEqual(sum, 15);
}
