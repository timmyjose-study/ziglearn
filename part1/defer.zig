const std = @import("std");
const testing = std.testing;

test "defer" {
    var x: u8 = 5;
    {
        defer x *= 2;
        testing.expectEqual(x, 5);
    }
    testing.expectEqual(x, 10);
}

test "multiple defer" {
    var x: f32 = 5;
    {
        defer x += 2;
        defer x /= 2;
    }
    testing.expectEqual(x, 4.5);
}
