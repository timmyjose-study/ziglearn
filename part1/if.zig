const std = @import("std");
const testing = std.testing;

test "if" {
    var x = false;
    var val: i32 = 99;

    if (x) {
        val += 1;
    } else {
        val += 2;
    }

    testing.expectEqual(val, 101);
}

test "if exprssions" {
    const a = true;
    var x: u16 = 0;

    x = if (a) 42 else 21;
    testing.expectEqual(x, 42);
}
